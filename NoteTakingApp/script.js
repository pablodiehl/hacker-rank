// Creates new notes
var menuItems = document.querySelectorAll(".color");


Array.from(menuItems).forEach(item => {
    item.addEventListener("click", function () {
        var notes = document.getElementById('notes'),
            newNote = document.createElement('div'),
            textArea = document.createElement('textarea'),
            removeIcon = document.createElement('button');
        
        notes.insertBefore(newNote, notes.firstChild);
        
        newNote.appendChild(textArea);
        textArea.onkeyup = function () {
            // This function is responsable for auto-resize the height of the note
            this.style.height = 0;
            this.style.height = textArea.scrollHeight - 3 + "px"; // That "- 3" is there to compasate the padding of the element
        };
        textArea.classList.add(this.classList[1]);
        textArea.focus();
        
        removeIcon.title = "Delete Note";
        removeIcon.onclick = function () {
            this.parentNode.remove();
        };
        removeIcon.classList.add(this.classList[1] + '-outline');
        
        newNote.appendChild(removeIcon);
    });
});