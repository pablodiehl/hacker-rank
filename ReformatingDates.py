# The challenge here is to reformat a list of dates from the format "20th Oct 2052" into the format "2052-10-20".
# Input:
#   20th Oct 2052
#   6th Jun 1933
#   26th May 1960
#   20th Sep 1958
#   16th Mar 2068
#   25th May 1912
#   16th Dec 2018
#   26th Dec 2061
#   4th Nov 2030
#   28th Jul 1963
# Output:
#   2052-10-20
#   1933-06-06
#   1960-05-26
#   1958-09-20
#   2068-03-16
#   1912-05-25
#   2018-12-16
#   2061-12-26
#   2030-11-04
#   1963-07-28

MONTH_DIGITS = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06',
                'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
def reformatDate(dates):
    # I decided to write a code which doesn't depend on external libraries,
    # so it might look like a bit "brutish"
    for index, date in enumerate(dates):
        # First, let's split the date into three pieces: day, month and year
        date = date.split()
        # Then, let's keep only the digits from the day string
        date[0] = ''.join([character for character in date[0] if character.isdigit()])
        # Adds a leading zero for the day value, if needed
        date[0] = date[0].rjust(2, '0')
        # Converts the month from abbreviated string to digits
        date[1] = MONTH_DIGITS[date[1]]
        # Re-joins the array into a string, ordering the values in the required format (yyyy-mm-dd)
        dates[index] = '-'.join(date[::-1])

    return dates
