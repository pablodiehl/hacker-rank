# The challenge here was print the X first non-prime numbers

# My code
def manipulate_generator(generator, n):
  is_not_prime = False
  # Let's iterate through the numbers until we find the next non-prime value 
  while(not is_not_prime):
    # Incrementing the number here because we print the previous value of the generator
    n += 1 
    # Here's were we check if the number is prime or not
    if not all(n % i for i in range(2, n)):
      is_not_prime = True
    else:
      next(generator)

# Base code provided in the challenge
def positive_integers_generator():
    n = 1
    while True:
        x = yield n
        if x is not None:
            n = x
        else:
            n += 1

k = int(input())
g = positive_integers_generator()
for _ in range(k):
    n = next(g)
    print(n)
    manipulate_generator(g, n)
