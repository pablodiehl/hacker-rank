# The challenge here was to consume an API (https://jsonmock.hackerrank.com/api/stocks/) and prints the data based on three informations
# First Date, Last Date and Week Day.


# Base code provided in the challenge
import sys
import os
from urllib.request import Request
from urllib.request import urlopen
from urllib.error import URLError
import json


# My code
import datetime

BASE_URL = "https://jsonmock.hackerrank.com/api/stocks/search?date="
DATE_FORMAT = "%d-%B-%Y"
def openAndClosePrices(firstDate, lastDate, weekDay):
    # First we convert the input dates into datetime objects
    firstDate = datetime.datetime.strptime(firstDate, "%d-%B-%Y")
    lastDate = datetime.datetime.strptime(lastDate, "%d-%B-%Y")
    current_date = firstDate
    # Iterates each month between first and last dates
    while(current_date < lastDate):
        month_to_get = current_date.strftime("%d-%B-%Y")
        # The two following lines will transform the date
        # from "1-January-2000" into just "January-2000"
        month_to_get = month_to_get.split('-')
        month_to_get = '-'.join([month_to_get[1], month_to_get[2]])
        stock_request = urlopen(BASE_URL + month_to_get)  # Gets the stocks for the whole month
        data = stock_request.read()
        encoding = stock_request.info().get_content_charset('utf-8')
        data = json.loads(data.decode(encoding))
        for item in data["data"]:
            item_date = datetime.datetime.strptime(item['date'],  '%d-%B-%Y')
            if item_date >= firstDate and item_date <= lastDate and item_date.strftime('%A') == weekDay:
                print("{} {} {}".format(item["date"], item["open"], item["close"]))

        current_date += datetime.timedelta(31) # Move 31 days forward

# Base code provided in the challenge
try:
    _firstDate = str(input())
except:
    _firstDate = None


try:
    _lastDate = str(input())
except:
    _lastDate = None


try:
    _weekDay = str(input())
except:
    _weekDay = None

openAndClosePrices(_firstDate, _lastDate, _weekDay)

