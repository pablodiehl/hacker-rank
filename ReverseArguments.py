# The challenge here was to run a function with arguments reversed

# My code
from functools import wraps

def reversed_args(f):
    # Replaces the original function for a new one, with inverting the arguments
    @wraps(f)
    def performs_reversing(*args):
      return f(*args[::-1])

    return performs_reversing

# Base code provided in the challenge
int_func_map = {
    'pow': pow,
    'cmp': lambda a, b: 0 if a == b else [1, -1][a < b],
}

string_func_map = {
    'join_with': lambda separator, *args: separator.join(args),
    'capitalize_first_and_join': lambda first, *args: ''.join([first.upper()] + list(args)),
}

queries = int(input())
for _ in range(queries):
    line = input().split()
    func_name, args = line[0], line[1:]
    if func_name in int_func_map:
        args = list(map(int, args))
        print(reversed_args(int_func_map[func_name])(*args))
    else:
        print(reversed_args(string_func_map[func_name])(*args))
